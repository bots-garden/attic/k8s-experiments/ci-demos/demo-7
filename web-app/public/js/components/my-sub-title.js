Vue.component(`my-sub-title`, {
  template: `
    <h2>
      {{subtitle}}
    </h2>
  `,
  data() {
    return {
      subtitle: "Devoxx FR", timer: null
    }
  },
  created() {
    this.refresh()
    this.timer = setInterval(this.refresh, 3000)
  },
  methods:{
    refresh: function() {
      axios.get(`${new URL(window.location.href).origin}/function/say-yo`).then(response => {
        this.subtitle = response.data.title
      })
      .catch(function (error) {
        this.subtitle = error.message
      })
    }
  },
  mounted() {
    console.log("⚡️ the my-sub-title vue is mounted")
  }
})

