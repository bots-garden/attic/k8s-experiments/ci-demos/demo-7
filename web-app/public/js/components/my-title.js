Vue.component(`my-title`, {
  template: `
    <div>
      <h1>
        {{title}}
      </h1> 
    </div>
  `,
  data() {
    return {
      title: "OpenFaas", timer: null
    }
  },
  created() {
    this.refresh()
    this.timer = setInterval(this.refresh, 3000)
  },
  methods:{
    refresh: function() {
      axios.get(`${new URL(window.location.href).origin}/function/say-hello`).then(response => {
        this.title = response.data.title
      })
      .catch(function (error) {
        this.title = error.message
      })
    }
  },
  mounted() {
    console.log("⚡️ the my-title vue is mounted")
    this.$root.$on("ping", (message)=> {
      console.log(`🎉 ${message} loaded`)
    })
  }
});
